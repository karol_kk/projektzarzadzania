<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.*, model.*, controller.*, util.*"
%>

<%
	if (session.getAttribute("login") == null) {
		response.sendRedirect("index.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Strona główna</title>
</head>
<body>
	<h1>
		Welcome ${sessionScope.login}!
		<%
		session.getAttribute("login");
	%>
	</h1>
	<a href="./LoginServlet?action=logout">Wyloguj</a>
	<a href="./add.jsp">Dodaj zadanie</a>
	

	<table border=1 cellpadding=2 cellspcacing=0 width="100%">
		<thead>
			<tr>
				<th>#</th>
				<th>Tytuł</th>
				<th>Treść</th>
				<th>Akcje</th>
			</tr>
		</thead>
		<tbody>
		<%
		TaskController tController = new TaskController();
		List<TaskModel> tasksList = tController.getTasksByUserId((int) session.getAttribute("userId"));
		int columnId=1;
		for(TaskModel tm: tasksList){
			//TaskModel tm = (TaskModel) tempTask;
		%>
			<tr>
				<td><% out.print(columnId); %></td>
				<td><% out.print(tm.getSubject()); %></td>
				<td><% out.print(tm.getContent()); %> </td>
				<td><a href="./editTask.jsp?action=edit&id=<% out.print(tm.getId()); %>">Edytuj</a>&bull;
				 	<a href="./TaskServlet?action=delete&id=<% out.print(tm.getId()); %>">Usuń</a></td>
				
		

			<%
			columnId++;
			}
		%>
			
		</tbody>

	</table>
</body>
</html>