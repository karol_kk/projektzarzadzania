<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="model.*, controller.*"
    
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
int taskId = Integer.parseInt(request.getParameter("id"));
TaskController tController = new TaskController();
TaskModel currentTask = tController.getTaskById(taskId);
%>

<h2>Edycja zadania:</h2>
<form action="./TaskServlet" method="POST">
<label>Tytuł:</label>
<input type="text" name="subject" value="<% out.print(currentTask.getSubject()); %>" />
<label>Treść:</label>
<textarea name="content"><% out.print(currentTask.getContent()); %></textarea>

<input type="hidden" name="action" value="edit" />
<input type="hidden" name="id" value="<% out.print(currentTask.getId()); %>" />
<input type="submit" value="Edytuj" />
</form>

</body>
</html>