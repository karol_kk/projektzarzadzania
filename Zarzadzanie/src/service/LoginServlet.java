package service;

import model.*;
import controller.*;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginServlet() {
		super();
		
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		HttpSession httpSession = request.getSession();
		if (action.equalsIgnoreCase("logout")) {
			httpSession.removeAttribute("login");
			response.sendRedirect("index.jsp");
			//response.sendRedirect("home.jsp");

		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		LoginController loginC = new LoginController();
		HttpSession httpSession = request.getSession();
		if (action.equalsIgnoreCase("login")) {
			int userId = loginC.getUserIdFromDBByLoginAndPassword(request.getParameter("login"), request.getParameter("password"));
			if (userId != -1) {
				httpSession.setAttribute("userId", userId);
				httpSession.setAttribute("login", request.getParameter("login"));
				response.sendRedirect("welcome.jsp");
			} else {
				response.sendRedirect("error.jsp");
			}

		}else if(action.equalsIgnoreCase("register")) {
			if(request.getParameter("password").equals(request.getParameter("password2"))){
				loginC.addUser(request.getParameter("login"), request.getParameter("password"));
				response.sendRedirect("index.jsp");

			}
		}
		

	}

}
