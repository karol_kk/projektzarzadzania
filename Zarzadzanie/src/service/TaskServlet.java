package service;

import java.io.IOException;
import controller.*;
import util.HibernateUtil;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

@WebServlet("/TaskServlet")
public class TaskServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public TaskServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		TaskController tController = new TaskController();
		if (action.equalsIgnoreCase("delete")) {
			int taskId = Integer.parseInt(request.getParameter("id"));
			tController.deleteTask(taskId);
			response.sendRedirect("welcome.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {;
		String action = request.getParameter("action");
		TaskController tController = new TaskController();
		
		if (action.equalsIgnoreCase("edit")) {
			int editId = Integer.parseInt(request.getParameter("id"));
			String editSubject = request.getParameter("subject");
			String editContent = request.getParameter("content");
			tController.editTaskById(editId, editSubject, editContent);
			response.sendRedirect("welcome.jsp");
		}
		
		if (action.equalsIgnoreCase("add")) {
			//int addId = Integer.parseInt(request.getParameter("id"));
			// userid z sesji
			int userId = (int) request.getSession().getAttribute("userId");
			String addSubject = request.getParameter("subject");
			String addContent = request.getParameter("content");
			tController.addTask(addSubject, addContent, userId);
			response.sendRedirect("welcome.jsp");
		}
	}

}
