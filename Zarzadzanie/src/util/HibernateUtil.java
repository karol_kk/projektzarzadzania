package util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import model.TaskModel;
import model.UserModel;

public class HibernateUtil {
	static SessionFactory sf;

	private HibernateUtil() {
	}

	public static Session getSession() {
		if (sf == null) {
			sf = new Configuration().configure().buildSessionFactory();
			fillExampleData();
		}
		return sf.openSession();
	}

	static private void fillExampleData() {
		Session s = HibernateUtil.getSession();
		Transaction t = s.beginTransaction();
		try {
			UserModel uModel = new UserModel(0, "Kuba", "haslo");
			TaskModel tModel1 = new TaskModel(0, "tytulzadania #1", "tresc #1", 1);
			TaskModel tModel2 = new TaskModel(0, "tytulzadania #2", "tresc #2", 1);
			TaskModel tModel3 = new TaskModel(0, "tytulzadania #3", "tresc #3", 1);
			s.save(uModel);
			s.save(tModel1);
			s.save(tModel2);
			s.save(tModel3);
			t.commit();
		} catch (Exception e) {
			if (t != null) {
				t.rollback();
			}
			e.printStackTrace();
		}
		s.close();
	}
}
