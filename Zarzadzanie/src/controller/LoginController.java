package controller;

import util.*;
import org.hibernate.Session;
import org.hibernate.Transaction;

import model.*;
import java.util.*;

public class LoginController {
	public LoginController() {
	}

	// TODO: int getUserIdFromDBByLoginAndPassword
	// -1  -gdy nie ma w bazie
	// id z bazy gdy jest
	public int getUserIdFromDBByLoginAndPassword (String username, String password) {
        Session session = HibernateUtil.getSession();
        int userId = -1;
        List<UserModel> usersList = new ArrayList<>();
        
        try {
            usersList = session.createSQLQuery("SELECT * FROM users "
                                 + "WHERE login = '" + username + "' "
                                 + "AND password = '" + password + "'").addEntity(UserModel.class).list();
            System.out.println("User List" + usersList);
            if(usersList.size() == 1) {
                userId = usersList.get(0).getId();
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return userId;
	}

	public void addUser(String login, String password) {
		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			UserModel userM = new UserModel(0, login, password);
			session.save(userM);
			transaction.commit();

		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();

		}
	}
}
