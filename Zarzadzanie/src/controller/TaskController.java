package controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.TaskModel;
import model.UserModel;
import util.HibernateUtil;

public class TaskController {
	public List<TaskModel> getTasksByUserId(int userId) {
		Session session = HibernateUtil.getSession();
		List<TaskModel> tasksList = new ArrayList<>();
		try {
			tasksList = session.createQuery("FROM TaskModel WHERE userId = " + userId).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return tasksList;
	}

	public void deleteTask(int taskId) {
		Session session = HibernateUtil.getSession();
		Transaction t = session.beginTransaction();
		try {
			session.delete(session.get(TaskModel.class, taskId));
			t.commit();
		} catch (Exception e) {
			if (t != null) {
				t.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public TaskModel getTaskById(int taskId) {
		Session session = HibernateUtil.getSession();
		TaskModel tModel = null;

		try {
			tModel = session.get(TaskModel.class, taskId);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return tModel;

	}

	public void editTaskById(int id, String subject, String content) {
		Session session = HibernateUtil.getSession();
		Transaction t = session.beginTransaction();
		try {
			TaskModel tempTask = session.get(TaskModel.class, id);
			tempTask.setSubject(subject);
			tempTask.setContent(content);
			session.update(tempTask);
			t.commit();
		} catch (Exception e) {
			if (t != null) {
				t.rollback();
			}
		} finally {
			session.close();
		}

	}
	
	public void addTask(String subject, String content, int userId) {
		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			//TaskModel taskM = session.get(TaskModel.class, userId);	
			TaskModel taskM = new TaskModel();
			taskM.setSubject(subject);
			taskM.setContent(content);
			taskM.setUserId(userId);
			session.persist(taskM);
			transaction.commit();

		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();

		}
	}
}
